require('dotenv').config();

const cluster = require('cluster');
const numCPUs = require('os').cpus().length;
const puppeteer = require('puppeteer');
const FormData = require('form-data');
const lighthouse = require('lighthouse');
const branchName = require('current-git-branch');
const fetch = require('isomorphic-fetch');
const fs = require('fs');

const LOGIN_URL = '/login';
const USER_EMAIL = process.env.USER_EMAIL;
const USER_PASSWORD = process.env.USER_PASSWORD;
const DOMAIN = process.env.DOMAIN || 'https://werkspot.nl';
const GITLAB_DOMAIN = 'gitlab.com';
const NAME_SPACE = 'silentimp';
const PROJECT = 'lighthouse-comment';
const JOB_NAME = 'lighthouse';
const GITLAB_PROJECT_ID = '15506600';
const TOKEN = process.env.GITLAB_TOKEN;

// urls
const urls = [
  '/inloggen',
  '/wachtwoord-herstellen-otp',
  '/lp/service',
  '/stucen',
  '/send-request-to/ww-tammer',
  '/post-service-request/binnenschilderwerk',
];

// to authentication
// don't need at the moment, but shouldn't deletex
// eslint-disable-next-line no-unused-vars
const login = async browser => {
  const page = await browser.newPage();
  page.setCacheEnabled(false);
  await page.goto(`${DOMAIN}${LOGIN_URL}`, { waitUntil: 'networkidle2' });
  await page.click('input[name=email]');
  await page.keyboard.type(USER_EMAIL);
  await page.click('input[name=password]');
  await page.keyboard.type(USER_PASSWORD);
  await page.click('button[data-testid="submit"]', { waitUntil: 'domcontentloaded' });
};

// build report for single url
const buildReport = browser => async url => {
  const data = await lighthouse(
    `${DOMAIN}${url}`,
    {
      port: new URL(browser.wsEndpoint()).port,
      output: 'json',
    },
    {
      extends: 'lighthouse:full',
    }
  );
  const { report: reportJSON } = data;
  const report = JSON.parse(reportJSON);
  const metrics = [
    {
      name: report.categories.performance.title,
      value: report.categories.performance.score,
      desiredSize: 'larger',
    },
    {
      name: report.categories.accessibility.title,
      value: report.categories.accessibility.score,
      desiredSize: 'larger',
    },
    {
      name: report.categories['best-practices'].title,
      value: report.categories['best-practices'].score,
      desiredSize: 'larger',
    },
    {
      name: report.categories.seo.title,
      value: report.categories.seo.score,
      desiredSize: 'larger',
    },
    {
      name: report.categories.pwa.title,
      value: report.categories.pwa.score,
      desiredSize: 'larger',
    },
  ];
  return {
    subject: url,
    metrics: metrics,
  };
};

/**
 * Возвращает текст комментария содержащий таблицу 
 * с изменениями метрик производительности
 *
 * @param branch {Object} объект с отчетом из фичер-бранча
 * @param master {Object} объект с отчетом из ветки master
 * @return {String} маркдаун для комментария
 */
const getArtifact = async name => {
  const path = `https://${GITLAB_DOMAIN}/${NAME_SPACE}/${PROJECT}/-/jobs/artifacts/master/raw/${name}?job=${JOB_NAME}`;
  const response = await fetch(path);
  if (!response.ok) throw new Error('Artifact not found');
  const data = await response.json();
  return data;
};

/**
 * Возвращает часть отчета, для конкретной страницы
 * 
 * @param report {Object} — отчет
 * @param subject {String} — subject, позволяющий найти конкретную страницу
 * @return {Object} — cтраница отчета
 */
const getPage = (report, subject) => report.find(item => (item.subject === subject));

/**
 * Возвращает конкретную метрику для указанной страницы
 * 
 * @param page {Object} — конкретная метрика
 * @param name {String} — имя метрики
 * @return {Object} — метрика страницы отчета
 */
const getMetric = (page, name) => page.metrics.find(item => item.name === name);

/**
 * Возвращает ячейку таблицы для выбранного параметра
 * 
 * @param branch {Object} - отчет о производительности из фичер-бранча
 * @param master {Object} - отчет о производительности из мастера
 * @param name {String} - имя метрики
 */
const buildCell = (branch, master, name) => {
  const branchMetric = getMetric(branch, name);
  const masterMetric = getMetric(master, name);
  const branchValue = branchMetric.value;
  const masterValue = masterMetric.value;
  const desiredLarger = branchMetric.desiredSize === 'larger';
  const isChanged = branchValue !== masterValue;
  const larger = branchValue > masterValue;
  if (!isChanged) return `${branchValue}`;
  if (larger) return `${branchValue} ${desiredLarger ? '💚' : '💔' }&nbsp;**+${Math.abs(branchValue - masterValue).toFixed(2)}**`;
  return `${branchValue} ${!desiredLarger ? '💚' : '💔' }&nbsp;**-${Math.abs(branchValue - masterValue).toFixed(2)}**`;
};

/**
 * Возвращает текст комментария содержащий таблицу 
 * с изменениями метрик производительности
 *
 * @param branch {Object} объект с отчетом из фичер-бранча
 * @param master {Object} объект с отчетом из ветки master
 * @return {String} маркдаун для комментария
 */
const buildCommentText = (branch, master) =>{
  const md = branch.map( page => {
    const pageAtMaster = getPage(master, page.subject);
    if (!pageAtMaster) return '';
    const md = `|${page.subject}|${buildCell(page, pageAtMaster, 'Performance')}|${buildCell(page, pageAtMaster, 'Accessibility')}|${buildCell(page, pageAtMaster, 'Best Practices')}|${buildCell(page, pageAtMaster, 'SEO')}|
`;
    return md;
  }).join('');
  return `
|Path|Performance|Accessibility|Best Practices|SEO|
|--- |--- |--- |--- |--- |
${md}
`;
};



/**
 * Возвращает iid мердж-реквеста из выбранной ветки в мастер
 * @param branch {String} — имя ветки
 * @return {Number} — iid мердж-реквеста из выбранной ветки в мастер
 */
const getMRID = async branchName => {
  const MRPath = `https://${GITLAB_DOMAIN}/api/v4/projects/${GITLAB_PROJECT_ID}/merge_requests?target_branch=master&source_branch=${currentBranch}`;
  const response = await fetch(MRPath, {
    method: 'GET',
    headers: {
      'PRIVATE-TOKEN': TOKEN,
    }
  });
  const [{iid}] = await response.json();
  return iid;
};

/**
 * Добавляет комментарий в мердж-реквест
 * @param md {String} — текст комментария в формате markdown
 */
const addComment = async md => {
  const currentBranch = branchName();
  const iid = await getMRID(currentBranch);
  const commentPath = `https://${GITLAB_DOMAIN}/api/v4/projects/${GITLAB_PROJECT_ID}/merge_requests/${iid}/notes`;
  const body = new FormData();
  body.append('body', md);

  await fetch(commentPath, {
    method: 'POST',
    headers: {
      'PRIVATE-TOKEN': TOKEN,
    },
    body,
  });
};

/**
 * Returns an array with arrays of the given size.
 *
 * @param myArray {Array} array to split
 * @param chunk_size {Integer} Size of every group
 */
function chunkArray(myArray, chunk_size){
    var index = 0;
    var arrayLength = myArray.length;
    var tempArray = [];
    
    for (index = 0; index < arrayLength; index += chunk_size) {
        myChunk = myArray.slice(index, index+chunk_size);
        tempArray.push(myChunk);
    }

    return tempArray.filter(chunk => chunk.length > 0);
}

const MEASURES_COUNT = 3;
const mergeMetrics = (pages, page) => {
  if (!pages) return page;
  return {
    subject: pages.subject,
    metrics: pages.metrics.map((measure, index) => {
      let value = (measure.value + page.metrics[index].value)/2;
      value = +value.toFixed(2);
      return {
        ...measure,
        value,
      }
    }),
  }
}

// @TODO add multiple treads
// @TODO add pages for loged in users
(async () => {
  if (cluster.isMaster) {
    const arrays = chunkArray(urls, urls.length/numCPUs);
    let report = [];
    let reportsCount = 0;
    arrays.map(chank => {
      const worker = cluster.fork();
      worker.send(chank);
    });
    cluster.on('message', async (worker, msg) => {
      report = [...report, ...msg];
      worker.disconnect();
      reportsCount++;
      if (reportsCount === arrays.length) {
        fs.writeFileSync(`./performance.json`, JSON.stringify(report));

        try {
          const masterReport = await getArtifact('performance.json');
          const md = buildCommentText(report, masterReport)
          await addComment(md);
        } catch (error) {
          console.log(error);
        }

        process.exit(0);
      }
    });
    
  } else {
    process.on('message', async (urls) => {
      const browser = await puppeteer.launch({
        args: ['--no-sandbox', '--disable-setuid-sandbox', '--headless'],
      });
      const builder = buildReport(browser);
      const report = [];
      for (let url of urls) {
        let measures = [];
        let index = MEASURES_COUNT;
        while(index--){
          const metric = await builder(url);
          measures.push(metric);
        }
        const measure = measures.reduce(mergeMetrics);
        report.push(measure);
      }
      cluster.worker.send(report);
      await browser.close();
    });
  }

})();



